import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store, StoreModule as NgRxStoreModule} from  '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Dexie } from 'dexie';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormularioDestinoViajeComponent } from './components/formulario-destino-viaje/formulario-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesEstado, efectosDestinosViajes, inicializarDestinoViajeEstado, reductorDestinosViajes } from './models/destinos-viajes-estado.models';
import { ActionReducerMap } from '@ngrx/store';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from 'src/app/services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { InitMyDataAction } from './models/destinos-viajes-estado.models';
import { DestinoViaje } from './models/destino-viaje.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';

//Inicio AppConfig
export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//Fin AppConfig

//Inicio declaración de rutas
export const rutasHijasDeVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponentComponent }
];

const routes: Routes = [
  { path: '',  redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio',  component: ListaDestinosComponent },
  { path: 'destino/:id',  component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [ UsuarioLogueadoGuard ] },
  { path: 'vuelos', component: VuelosComponentComponent, canActivate: [ UsuarioLogueadoGuard ], children: rutasHijasDeVuelos }
]
//Fin declaración de rutas

// Inicia Redux
export interface EstadoApp {
  destinos: DestinosViajesEstado
}

const reducers: ActionReducerMap<EstadoApp> = {
  destinos: reductorDestinosViajes
};

let reductorEstadoInicial: any = {
  destinos: inicializarDestinoViajeEstado()
};

// Fin Redux

//Inicialización de la App
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  destinosDB: string[] = [];
  constructor(private store: Store<EstadoApp>, private http: HttpClient) { }

  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    //Esto carga desde el servidor app.js en localhost:3000/my
    //this.store.dispatch(new InitMyDataAction(response.body));
    
    //Y esto lo hace desde la Indexeddb
    const miDb = db;
    (await miDb.table('destinos').toArray()).forEach(dest => this.destinosDB.push(dest.nombre));
    this.store.dispatch(new InitMyDataAction(this.destinosDB));
  }
}
//Fin inicialización de la App

//Inicio Dexie DB
export class Traduccion {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({ providedIn: 'root' })
export class MiBase extends Dexie {
  
  constructor() {
    super('MiBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl'
    });

    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      traducciones: '++id, lang, key, value'
    });
  }
}

export const db = new MiBase();
//Fin Dexie DB

//Inicio i18n
class CargadorTraduccion implements TranslateLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promesa = db.table('traducciones')
                    .where('lang').equals(lang).toArray()
                    .then(resultados => {
                      if (resultados.length === 0) {
                        console.log("cargando traducciones");
                        return this.http.get<Traduccion[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                                .toPromise()
                                .then( resultadosAPI => {
                                  db.table('traducciones').bulkAdd(resultadosAPI);
                                  return resultadosAPI;
                                });
                      }  

                      return resultados;
                    }).then((traducciones: any) => {
                      console.log('traducciones cargadas: ');
                      console.log(traducciones);
                      return traducciones;
                    }).then((traducciones: any) => {
                      return traducciones.map((t: any) => ({ [t.key]: t.value}));
                    });
    
    return from(promesa).pipe(flatMap((elementos) => from(elementos)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new CargadorTraduccion(http);
}
//Fin i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormularioDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {initialState: reductorEstadoInicial, 
      runtimeChecks: { 
          strictStateImmutability: false, 
          strictActionImmutability: false,
      } }),
    EffectsModule.forRoot([efectosDestinosViajes]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthService, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService, { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MiBase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
