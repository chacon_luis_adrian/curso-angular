import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Una Lista de Deseos';
  time = new Observable(observador => {
    setInterval(() => observador.next(new Date().toString()), 1000);
  });

  constructor(public translate: TranslateService) {
    console.log('******************* get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }

  agregar(titulo: HTMLInputElement){
    console.log(titulo.value);
  }

  cambio(evento: any) {
    this.translate.use(evento.target.value);
  }
}
