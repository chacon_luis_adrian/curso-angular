import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { EstadoApp } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ResetearVotosAccion, VotarBienAccion, VotarMalAccion } from '../../models/destinos-viajes-estado.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input('indice') posicion: number; 
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() cliqueado: EventEmitter<DestinoViaje>;

  constructor(private store: Store<EstadoApp>) { 
    this.destino = new DestinoViaje('', '');
    this.posicion = 0;
    this.cliqueado = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir() {
    this.cliqueado.emit(this.destino);
    return false;
  }

  votarBien() {
    this.store.dispatch(new VotarBienAccion(this.destino));
    return false;
  }

  votarMal() {
    this.store.dispatch(new VotarMalAccion(this.destino));
    return false;
  }

  resetearVotos() {
    this.store.dispatch(new ResetearVotosAccion(this.destino));
    return false;
  }
}
