import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { EstadoApp } from '../../app.module';
import { ElegidoFavoritoAccion, NuevoDestinoAccion } from '../../models/destinos-viajes-estado.models';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAgregado: EventEmitter<DestinoViaje>;
  actualizaciones: string[];
  todos: any;

  constructor(public destinosApiCliente: DestinosApiClient, private store: Store<EstadoApp>) {
    this.onItemAgregado = new EventEmitter();
    this.actualizaciones = [];
  
    this.store.select(estado => estado.destinos.favorito)
    .subscribe(data =>{
       let fav = data;
      if (fav != null) {
        if (fav.nombre != 'vacio' && fav.esSeleccionado()) {
          this.actualizaciones.push('Ha seleccionado a ' + fav.nombre);
          console.log('despues del push, ' + fav.nombre + fav.id + fav.esSeleccionado());
        }
      }
    });

    this.store.select(estado => estado.destinos.items).subscribe(items => this.todos = items);
  }

  ngOnInit(): void {

  }

  agregado(d: DestinoViaje) {
    this.destinosApiCliente.agregarDestino(d);
    this.onItemAgregado.emit(d);
  }

  seleccionado(d: DestinoViaje){
    this.destinosApiCliente.elegirDestino(d);
  }
}
