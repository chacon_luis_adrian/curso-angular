import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-formulario-destino-viaje',
  templateUrl: './formulario-destino-viaje.component.html',
  styleUrls: ['./formulario-destino-viaje.component.css']
})
export class FormularioDestinoViajeComponent implements OnInit {
  @Output() onItemAgregado: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  resultadosBuscados: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAgregado = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.validaNombre,
        this.validaNombreParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
      console.log('es valido', this.fg.valid);
      console.log('nombre', this.fg.controls['nombre'].valid);
      console.log('url', this.fg.controls['url'].valid)
    });

    this.resultadosBuscados = [];
   }

  ngOnInit(): void {
    let nombreElemento = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(nombreElemento, 'input')
      .pipe(
        map((e: Event) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
      ).subscribe(respuestaAjax => this.resultadosBuscados = respuestaAjax.response);
/*         switchMap(() => ajax('/assets/datos.json'))
      ).subscribe(respuestaAjax => {
          console.log('respuesta Ajax');
          console.log(respuestaAjax);
          console.log('respuesta Ajax response');
          console.log(respuestaAjax.response);
          this.resultadosBuscados = respuestaAjax.response
          .filter(function(x: any){
            return x.toLowerCase().includes(nombreElemento.value.toLowerCase());
          });
     }); */
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAgregado.emit(d);
    return false;
  }

  validaNombre(control: FormControl): { [s: string]: boolean} | null{
    let longitud = control.value.toString().trim().length;

    if (longitud > 0 && longitud < 5) {
      return { NombreInvalido: true};
    }

    return null;
  } 

  validaNombreParametrizable(minLong: number): ValidatorFn {
     return (control: AbstractControl): {[s: string]: boolean} | null => {
      let longitud = control.value.toString().trim().length;
      
      if (longitud > 0 && longitud < minLong) {
        return { MinimaLongitudNombre: true};
      }

      return null;
    }
  }
}
