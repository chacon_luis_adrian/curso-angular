//import { Component, OnInit } from '@angular/core';
import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
 import { Store } from '@ngrx/store';
import { EstadoApp } from 'src/app/app.module'; 
import { HttpClient } from '@angular/common/http';

 class DestinoApiClientViejo {
  
  obtenerDestinoPorID(id: string): DestinoViaje {
    console.log('Llamando por la clase vieja');
    let paraError = new DestinoViaje('porError', 'urlError');
    return paraError;
  }
}

interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorated  {
  
  constructor(store: Store<EstadoApp>, @Inject(APP_CONFIG) private config: AppConfig, http: HttpClient) {
    //super(store, config, http);
  }

  obtenerDestinoPorID(id: string): DestinoViaje {
    console.log('Llamando por la clase decorada!');
    console.log('Config: ' + this.config.apiEndPoint);
    return new DestinoViaje(id, '');
  }
} 

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated},
    { provide: DestinoApiClientViejo, useExisting: DestinosApiClient}
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiCliente: DestinosApiClient) { 
    this.destino = new DestinoViaje('Nombre', 'Imagen');
  }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    id = id !== null ? id : '';
    this.destino = this.destinosApiCliente.obtenerDestinoPorID(id);
  }
}
