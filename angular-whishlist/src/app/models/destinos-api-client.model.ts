import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
//import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, APP_CONFIG, db, EstadoApp } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { ElegidoFavoritoAccion, NuevoDestinoAccion } from './destinos-viajes-estado.models';
import { HttpClient, HttpClientModule, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { NextObserver, Observable } from 'rxjs';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[]=[];
    
    constructor(private store: Store<EstadoApp>,
                @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
                private http: HttpClient) {
         this.store
            .select(estado => estado.destinos)
            .subscribe((data) => {
                console.log("destinos sub store");
                console.log(data);
                this.destinos = data.items;
            });
        
        this.store
            .subscribe((data) => {
                console.log("all store");
                console.log(data);
            }); 
    }

/*     agregarDestino(d: DestinoViaje) {
        this.store.dispatch(new NuevoDestinoAccion(d));
    } */

    async agregarDestino(d: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: headers});
        const response: any = await this.http.request(req).toPromise();

        if (response.status === 200) {
            this.store.dispatch(new NuevoDestinoAccion(d));
            const miBase = db;
            miBase.table('destinos').add(d);
            console.log('todos los destinos de la db');
            miBase.table('destinos').toArray().then(destinos => console.log(destinos));
        }

        /*subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                
            }
        });*/
    }

    obtenerDestinos(): DestinoViaje[] {
        return this.destinos;
    } 

    obtenerDestinoPorID(id: string): DestinoViaje {
        return this.destinos.filter(function(d) {return d.id.toString() === id;})[0];
    } 

    elegirDestino(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAccion(d));
    }
}