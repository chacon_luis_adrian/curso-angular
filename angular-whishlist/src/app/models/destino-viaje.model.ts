import { UUID } from 'uuid-generator-ts';

export class DestinoViaje{
    private seleccionado: boolean;
    public servicios: string[];
    public id: string;
    public votos: number;

    constructor(public nombre:string, public imagenUrl:string){
        this.seleccionado = false;
        this.servicios = ['Gimnasio', 'Casino'];
        const uuid = new UUID();
        this.id = uuid.getDashFreeUUID();
        this.votos = 0;
    }

    esSeleccionado(): boolean {
       return this.seleccionado;
    }

    asignaSeleccionado(seleccionado: boolean) {
        this.seleccionado = seleccionado;
    }

    votarBien() {
        this.votos++;
    }

    votarMal() {
        this.votos--;
    }

    resetearVotos() {
        this.votos = 0;
    }
}