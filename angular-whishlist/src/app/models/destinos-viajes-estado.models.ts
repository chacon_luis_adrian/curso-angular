import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { DestinoViaje } from "./destino-viaje.model";
import { HttpClientModule } from '@angular/common/http';

// El estado
export interface DestinosViajesEstado {
    items: DestinoViaje[];
    cargando: boolean;
    favorito: DestinoViaje;
}

export function inicializarDestinoViajeEstado() {
    return {
        items: [],
        cargando: false,
        favorito: null
    };
}

// Las acciones
export enum DestinoViajesTiposAccion {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTAR_BIEN = '[Destinos Viajes] Votar Bien',
    VOTAR_MAL = '[Destinos Viajes] Votar Mal',
    RESETEAR_VOTOS = '[Destinos Viajes] Resetear Votos',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAccion implements Action {
    type = DestinoViajesTiposAccion.NUEVO_DESTINO;

    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAccion implements Action {
    type = DestinoViajesTiposAccion.ELEGIDO_FAVORITO;

    constructor(public destino: DestinoViaje) {}
}

export class VotarBienAccion implements Action {
    type = DestinoViajesTiposAccion.VOTAR_BIEN;

    constructor(public destino: DestinoViaje) {}
}

export class VotarMalAccion implements Action {
    type = DestinoViajesTiposAccion.VOTAR_MAL;

    constructor(public destino: DestinoViaje) {}
}

export class ResetearVotosAccion implements Action {
    type = DestinoViajesTiposAccion.RESETEAR_VOTOS;

    constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action {
    type = DestinoViajesTiposAccion.INIT_MY_DATA;

    constructor(public destinos: string[]) {}
}

export type DestinoViajesAcciones = NuevoDestinoAccion | ElegidoFavoritoAccion | VotarBienAccion | VotarMalAccion
                                    | ResetearVotosAccion | InitMyDataAction; 

// Los reductores
export function reductorDestinosViajes (
    estado: DestinosViajesEstado | any,
    accion: DestinoViajesAcciones | any
): DestinosViajesEstado | any {
    switch (accion.type) {
        case DestinoViajesTiposAccion.NUEVO_DESTINO: {
            return {
                ...estado,
                items: [...estado.items, (accion as NuevoDestinoAccion).destino]
            };
        }
        
        case DestinoViajesTiposAccion.ELEGIDO_FAVORITO: {
            estado.items.forEach((x: DestinoViaje) => x.asignaSeleccionado(false));
            let fav: DestinoViaje = (accion as ElegidoFavoritoAccion).destino;
            fav.asignaSeleccionado(true);
            return {
                ...estado,
                favorito: fav
            };
        }
        case DestinoViajesTiposAccion.VOTAR_BIEN: {
            let d: DestinoViaje = (accion as VotarBienAccion).destino;
            d.votarBien();
            return {...estado};
        }
        case DestinoViajesTiposAccion.VOTAR_MAL: {
            let d: DestinoViaje = (accion as VotarMalAccion).destino;
            d.votarMal();
            return {...estado};
        }
        case DestinoViajesTiposAccion.RESETEAR_VOTOS: {
            let d: DestinoViaje = (accion as ResetearVotosAccion).destino;
            d.resetearVotos();
            return {...estado};
        }
        case DestinoViajesTiposAccion.INIT_MY_DATA: {
            const destinos: string[] = (accion as InitMyDataAction).destinos;
            return {
                ...estado,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            };
        }
        default: {
            return estado
        }
    }
}

// Los efectos
@Injectable()
export class efectosDestinosViajes {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.acciones$.pipe(
        ofType(DestinoViajesTiposAccion.NUEVO_DESTINO),
        map((accion: NuevoDestinoAccion) => new ElegidoFavoritoAccion(accion.destino))
    );

    constructor(private acciones$: Actions) {}
}